#include <SPI.h>
#include <NeoPixelBus.h>

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <MFRC522.h>


// CONFIG HERE
const char* DEVICE = "esp-door-test";

const char IDENTIFIER[37] = "door-test";

const char* WIFI_SSID = "SSID";
const char* WIFI_PASS = "PASSWORD";

const char* MQTT_HOST = "10.0.0.6";
const char* MQTT_REQ_TOPIC = "commands";
const char* MQTT_RESP_TOPIC = "devices";
const int MQTT_RECONNECT_INTERVAL = 4000;

const int UPDATE_INTERVAL = 2000;

// Pin config
#define RST_PIN    D0
#define SS_PIN     D8 // SDA / SS
const int RELAY_PIN = D1;
const int LED_PIN = D2;
const int BUTTON_PIN = D3;
const int MAGNET_PIN = D4;

// DONT MODIFY BELOW

// Credential variables
int lastReadMillis = 0;
char idBuffer[4];
long lastCredOpen = 0;

// Network stuff
char msgBuffer[200];
int requestFailures = 0;
int requestFailureMillis = 0; // Time since the last verify failure
int lastConnAttempt = 0;

// General system vars
bool isOnline = false;

// door open button
int buttonState;
int lastButtonState = LOW;
long lastDebounceTime = 0;
long debounceDelay = 50;
long lastButtonOpen = 0; // When was the door last opened... we use this to keep track of when to close the relay


// Door open or closed status
int doorState = 0;

// Last status update
long lastStatusUpdate = 0;

// Neopixel status LED's
#define colorSaturation 128

RgbColor red = RgbColor(colorSaturation, 0, 0);
RgbColor green = RgbColor(0, colorSaturation, 0);
RgbColor blue = RgbColor(0, 0, colorSaturation);
RgbColor white = RgbColor(colorSaturation);
RgbColor black = RgbColor(0);

NeoPixelBus statusLeds = NeoPixelBus(2, LED_PIN);

int systemLed = 0;
int readerLed = 1;


RgbColor blinkWithColor[2] = {};
bool blinkState[2] = {false};
int blinkMillis[2] = {0};
int blinkInterval[2] = {0};

// Clients
WiFiClient espClient;
PubSubClient client(espClient);


MFRC522 mfrc522(SS_PIN, RST_PIN);


void setup() {
  char str[200];

  Serial.begin(115200);
  while (!Serial);

  sprintf(str, "Starting device with point %s", IDENTIFIER);
  Serial.println(str);

  // Sets LED to RED when it's bork
  statusLeds.Begin();
  statusLeds.SetPixelColor(systemLed, red);
  statusLeds.SetPixelColor(readerLed, red);
  statusLeds.Show();

  // Door solenoid pin
  pinMode(RELAY_PIN, OUTPUT);

  // Door opener button
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  // Magnet pin
  pinMode(MAGNET_PIN, INPUT_PULLUP);

  SPI.begin();
  mfrc522.PCD_Init();
  ShowReaderDetails();

  setup_wifi();

  client.setServer(MQTT_HOST, 1883);
  client.setCallback(callback);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  WiFi.begin(WIFI_SSID, WIFI_PASS);

  if (WiFi.status() != WL_CONNECTED) {
    return;
  }
  
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println("");

  char *cstring = (char *) payload; // NOT WORKING
  cstring[length]  = '\0';

  if (strcmp(cstring, "OPEN") == 0) {
    // Indicate that the door is opened
    statusLeds.SetPixelColor(readerLed, green);
    statusLeds.Show();
    digitalWrite(RELAY_PIN, HIGH);

    delay(4000);

    statusLeds.SetPixelColor(readerLed, white);
    statusLeds.Show();
    digitalWrite(RELAY_PIN, LOW);

    lastReadMillis = 0;
  } else if (strcmp(cstring, "DENIED") == 0) {
    statusLeds.SetPixelColor(readerLed, red);
    statusLeds.Show();
    delay(2000);
    statusLeds.SetPixelColor(readerLed, white);
    statusLeds.Show();
    
    lastReadMillis = 0;
  } else if (strcmp(cstring, "ACK_STATUS") == 0) {
  }
}

void ShowReaderDetails() {
  // Get the MFRC522 software version
  byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);
  Serial.print(F("MFRC522 Software Version: 0x"));
  Serial.print(v, HEX);
  if (v == 0x91) {
    Serial.print(F(" = v1.0"));
  } else if (v == 0x92) {
    Serial.print(F(" = v2.0"));
  } else {
    Serial.print(F(" (unknown)"));
  }
  Serial.println("");
  // When 0x00 or 0xFF is returned, communication probably failed
  if ((v == 0x00) || (v == 0xFF)) {
    Serial.println(F("WARNING: Communication failure, is the MFRC522 properly connected?"));
  }
}

void buttonOpen() {
  int reading = digitalRead(BUTTON_PIN);
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:
    // if the button state has changed:

    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == LOW) {
        lastButtonOpen = millis();
        digitalWrite(RELAY_PIN, HIGH);
      }
    }
  }

  lastButtonState = reading;
  
  if ((millis() - lastButtonOpen) > 5000) {
    digitalWrite(RELAY_PIN, LOW);
    lastButtonOpen = 0;
  }
}

bool getCardId() {
  // Getting ready for Reading PICCs
  if ( ! mfrc522.PICC_IsNewCardPresent()) { //If a new PICC placed to RFID reader continue
    return false;
  }
  if ( ! mfrc522.PICC_ReadCardSerial()) {   //Since a PICC placed get Serial and continue
    return false;
  }
  // There are Mifare PICCs which have 4 byte or 7 byte UID care if you use 7 byte PICC
  // I think we should assume every PICC as they have 4 byte UID
  // Until we support 7 byte PICCs
  Serial.println(F("Scanned PICC's UID:"));

  for (byte i = 0; i < mfrc522.uid.size; i++) {
    idBuffer[i] = mfrc522.uid.uidByte[i];
    Serial.print(idBuffer[i], HEX);
    Serial.print(" ");
  }

  Serial.println("");

  mfrc522.PICC_HaltA(); // Stop reading

  // # Store the last read..
  lastReadMillis = millis();
  return true;
}

/* 
 *  Method to handle opening via Credential like RFID / NFC, 
 *  this reads a card or so on and post to MQTT. The callback
 *  then handles the response from the Controller server and 
 *  grants a door open or a denial.
*/
void credReadAndVerify() {
    if ((lastReadMillis == 0) && (isOnline)) {
    bool status = getCardId();

    if (status) {
      statusLeds.SetPixelColor(readerLed, blue);
      statusLeds.Show();

      char id[100] = {};
      int cardIdLen = 0;
      get_uuid_as_string(id, cardIdLen);

      char req[400] = {};
      sprintf(req, "VERIFY;point=%s,device=%s,credential=%s", IDENTIFIER, DEVICE, id);

      client.publish(MQTT_REQ_TOPIC, req);
      Serial.println(req);
    }
  }
}

void checkVerifyTimedOut() {
  // If we dont get a response within 5 seconds we fail the current request...
  if ((lastReadMillis) && (isOnline)) {
    if ((millis() - lastReadMillis) > 1000) {
      Serial.println("Failed verification");
      statusLeds.SetPixelColor(readerLed, red);
      statusLeds.Show();
  
      requestFailureMillis = millis();
      requestFailures++;
      isOnline = false;
    }
  }

  if ((requestFailureMillis != 0) && (millis() - requestFailureMillis) > 2000) {
    statusLeds.SetPixelColor(readerLed, white);
    statusLeds.Show();
    
    isOnline = true;
    lastReadMillis = 0;
    requestFailureMillis = 0;
  }
}

void blinkLed(int led, int interval, RgbColor color) {
  blinkInterval[led] = interval;
  blinkWithColor[led] = color;  
}

void blinkStop(int led) {
  blinkMillis[led] = 0;
}

void blinkLeds() {
  for (int i = 0; i < 2; i++) {
    if (blinkInterval[i] != 0) {
      if ((millis() - blinkMillis[i]) > blinkInterval[i]) {
        if (blinkState[i]) {
          statusLeds.SetPixelColor(i, blinkWithColor[i]);
        } else {
          statusLeds.SetPixelColor(i, black);
        }

        blinkState[i] = !blinkState[i];
      }
    }
    blinkMillis[i] = millis();
  }
  statusLeds.Show();
}

void loop() {
  buttonOpen();

  // No "online" functionality without wifi...
  if (WiFi.status() == WL_CONNECTED) {
    if (!client.connected()) {
      reconnect();
      return;
    }

    credReadAndVerify();
    checkVerifyTimedOut();
    
    updateDoorStatus();
    postStatus();
    
    client.loop();
  }
}

void reconnect() {
  // Loop until we're reconnected 
  if ((!client.connected()) && (millis() - lastConnAttempt) > MQTT_RECONNECT_INTERVAL) {
    Serial.println("Attempting MQTT connection...");
    lastConnAttempt = millis();
    
    if (client.connect(DEVICE)) {
      char topic[50];
      sprintf(topic, "%s/%s", MQTT_RESP_TOPIC, DEVICE);
      client.subscribe(topic);
      
      isOnline = true;

      statusLeds.SetPixelColor(systemLed, white);
      statusLeds.SetPixelColor(readerLed, white);
      statusLeds.Show();

      Serial.println("Ready for work!");
    } else {
      isOnline = false;

      statusLeds.SetPixelColor(systemLed, red);
      statusLeds.SetPixelColor(readerLed, red);
      statusLeds.Show();

      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
    }
  }
}


/*
 * Handles updates about the door states
 */
void updateDoorStatus() {
  doorState = digitalRead(MAGNET_PIN);
}

/*
 * Post status about Door closed state etc to the controller via MQTT.
 */
void postStatus() {
  char buf[100] = {};
  if ((millis() - lastStatusUpdate) > UPDATE_INTERVAL) {
    sprintf(buf, "UPDATE_STATUS;open=%d,device=%s,point=%s", digitalRead(MAGNET_PIN), DEVICE, IDENTIFIER);
    client.publish(MQTT_REQ_TOPIC, buf);
    lastStatusUpdate = millis();
  }
}

void get_uuid_as_string(char* result, const unsigned result_length) {
  char part[4] = {};
  for (int i = 0; i < mfrc522.uid.size; i++) {
    sprintf(part, "%02x", idBuffer[i]);
    strcat(result, part);
  }
}
