# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import argparse
import logging
import sys
import time

import paho.mqtt.client as mqtt

from gatekeeper.common import conf
from gatekeeper import controller

logging.basicConfig(level="DEBUG")

client = mqtt.Client()

response = None


def on_connect(client, userdata, flags, rc):
    client.subscribe(conf.CFG["mqtt_device_topic"] % {"device": conf.CFG["device"]})
    client.publish(conf.CFG["mqtt_command_topic"], msg % data)


def on_message(client, userdata, msg):
    global response
    response = msg


if __name__ == '__main__':
    conf.load_config(prefix="GK")
    conf.set_defaults(controller.DEFAULTS)
    print conf.CFG

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('device', type=str)
    parser.add_argument('point', type=str)
    parser.add_argument('credential', type=str)
    parser.add_argument('max_wait', type=int, default=5)

    args = parser.parse_args()

    conf.CFG["device"] = args.device
    data = {
        "point": args.point,
        "device": args.device,
        "credential": args.credential
    }

    msg = "VERIFY;point=%(point)s,device=%(device)s,credential=%(credential)s"

    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(conf.CFG['mqtt_host'], conf.CFG['mqtt_port'])

    i = 0
    while i < args.max_wait:
        if response is not None:
            print(response.payload)
            break
        client.loop()
        time.sleep(0.1)
        i += 0.1
