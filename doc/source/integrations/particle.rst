Particle Cloud
==============

.. note:: The MQTT bus is not involved when using the particle cloud


Setting up your particle cloud
------------------------------

1. Configure a `oauth` app for gatekeeper:

.. note::

  In the example here we use the python based *httpie* utility instead of curl.

.. code-block:: bash

  $ http --debug --json https://api.particle.io/v1/clients access_token==<your access token> name=gatekeeper type=installed

2. Configure *client_id* and *client_secret* in your gatekeeper.ini

.. code-block:: text

  [integration.particle]
  client_id = <client id>
  client_secret = <client_secret>

3. Generate the token to be used in the webhooks

.. note::

  Generate a 40 char length random string to be used as a token

.. code-block:: bash

  $ mkpasswd.pl -s 0 -l 40

4. Set the token in the gatekeeper.ini configuration file.

This is used to validate the incoming webhook

.. code-block:: plain

  [integration.particle]
  webhook_token = <your token>

5. Configure the webhook in Particle

.. note::

  Replace the <TOKEN> in the json before doing the command below and change to
  the url of your gatekeeper instance.

.. code-block:: bash

  $ particle webhook create $gatekeeper_ctrl_photon_repo/webhooks/verify.json


Flow diagram
------------

.. image:: particle-flowdiag.txt.png
