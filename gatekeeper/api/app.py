# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import falcon
import falcon_cors

from gatekeeper.api.middleware import json_middleware
from gatekeeper.api.middleware import jwt_auth
from gatekeeper.api import command
from gatekeeper.api import credential
from gatekeeper.api import events
from gatekeeper.api import grant
from gatekeeper.api import point
from gatekeeper.api import particle
from gatekeeper.api import user
from gatekeeper.api import utils
from gatekeeper.common import conf


ALLOWED_ORIGINS = ['http://localhost:8080']  # Or load this from a config file
ALLOWED_HEADERS = ["Authorization", "Content-Type"]

DEFAULTS = {
    "auth_enable": True,
    "cors_allowed_origins": ALLOWED_ORIGINS,
    "cors_allowed_headers": ALLOWED_HEADERS,
}


def get_app():
    conf.set_defaults(DEFAULTS)

    middleware = [
        json_middleware.RequireJSON(),
        json_middleware.JSONTranslator()
    ]

    cors = falcon_cors.CORS(
        allow_origins_list=conf.get_list_opt("cors_allowed_origins"),
        allow_headers_list=conf.get_list_opt("cors_allowed_headers"),
        allow_all_methods=True
    )

    middleware.append(cors.middleware)

    if conf.CFG["auth_enable"]:
        middleware.append(
            jwt_auth.JwtMiddleware(conf.CFG)
        )

    api = falcon.API(
        request_type=utils.Request,
        response_type=utils.Response,
        middleware=middleware)

    api.add_route("/users", user.UsersResource())
    api.add_route("/users/{user_id}", user.UserResource())

    api.add_route("/credentials", credential.CredentialsResource())
    api.add_route("/credentials/{credential_id}",
                  credential.CredentialResource())

    api.add_route("/events", events.EventResource())

    api.add_route("/grants", grant.GrantsResource())
    api.add_route("/grants/{grant_id}", grant.GrantResource())

    api.add_route("/points", point.PointsResource())
    api.add_route("/points/{point_id}", point.PointResource())

    api.add_route("/verify", command.VerifyResource())

    # Particle webhooks integration
    api.add_route("/particle", particle.ParticleHookResource(conf.CFG))

    return api
