# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import falcon

from gatekeeper.db import models
from gatekeeper.db import api
from gatekeeper.db import session as db_session


class UsersResource(object):
    def on_get(self, req, resp):
        session = db_session.get_session()
        q = session.query(models.User)
        rows = q.all()
        req.context["result"] = {"users": map(dict, rows)}

    def on_post(self, req, resp):
        obj = api.create_user(req.context["doc"])
        req.context["result"] = dict(obj)
        resp.status = falcon.HTTP_201


class UserResource(object):
    def on_delete(self, req, resp, user_id):
        models.User.delete(user_id)
        resp.status = falcon.HTTP_204

    def on_get(self, req, resp, user_id):
        with db_session.session_scope() as session:
            q = session.query(models.User)
            q = q.filter_by(id=user_id)
            obj = api.get_user(user_id)
            req.context["result"] = dict(obj)
