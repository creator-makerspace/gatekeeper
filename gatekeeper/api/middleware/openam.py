# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import logging

import falcon
import requests
from oslo_config import cfg

LOG = logging.getLogger(__name__)


cfg.CONF.register_opts([
    cfg.StrOpt("url", default="https://oam.creator.no/openam")
], group="openam")


class AuthMiddleware(object):
    def process_request(self, req, resp):
        if req.method == "OPTIONS":
            return

        # Token can be in the cookie or as X-Auth-Token
        token = req.cookies.get("iPlanetDirectoryPro", None)
        if token is None:
            LOG.warn("iPlanet token not found, attempting Authorization")
            token = req.headers.get("AUTHORIZATION", None)

        if token is None:
            LOG.warn("Token was not found in request")

            resp.status_code = 401
            resp.json = {"error": "Missing token"}

            raise falcon.HTTPUnauthorized(
                "Authentication required",
                description="Please provide a token")

        headers = {"Content-Type": "application/json"}

        url = cfg.CONF.openam.url + "/json/sessions/%s?_action=validate"
        response = requests.post(url % token, headers=headers)

        if response.status_code != 200:
            LOG.error("Couldn't get profile from OpenAM")
            raise falcon.HTTPUnauthorized(
                "Authentication required",
                description="Error retrieving token")

        token_data = response.json()
        if not token_data["valid"]:
            raise falcon.HTTPUnauthorized(
                "Authentication required",
                description="Error retrieving token")

        # Retrieve info about the user
        url = cfg.CONF.openam.url + "/json/users/%s" % token_data["uid"]
        headers = {"iplanetDirectoryPro": token}
        response = requests.get(url, headers=headers)

        req.token = token
        req.user = response.json()
