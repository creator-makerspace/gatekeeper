# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import base64
import json
import logging

import falcon
import jwt
import requests

DEFAULTS = {
    "jwt_algorithm": "RS256",
    "jwt_audience": "gatekeeper",
    "jwt_pk_from_token": True
}

LOG = logging.getLogger(__name__)

NO_AUTH = [
    "/particle"
]


class JwtMiddleware(object):
    def __init__(self, cfg):
        self.cfg = cfg
        self.jwks_cache = {}

    def resolve_token_jwks(self, payload):
        # Attempt to autodiscover the signing key from the "iss"
        # claim and jwk key id
        token_header = jwt.api_jws.get_unverified_header(payload)
        token_kid = token_header["kid"]

        if token_kid in self.jwks_cache:
            return self.jwks_cache[token_kid]

        token_unv = jwt.decode(payload, verify=False)
        iss = token_unv["iss"]

        resp = requests.get(iss + '/.well-known/openid-configuration',
                            verify=False)
        oidc_config = resp.json()

        key_url = oidc_config["jwks_uri"]
        resp = requests.get(key_url, verify=False)
        keys = resp.json()

        jwks_data = json.dumps(
            [i for i in keys["keys"] if i["kid"] == token_kid][0])
        alg = jwt.algorithms.RSAAlgorithm.from_jwk(jwks_data)
        self.jwks_cache[token_kid] = alg
        return alg

    def process_request(self, req, resp):
        if req.method == "OPTIONS":
            return

        if req.path in NO_AUTH:
            LOG.debug("Skipping auth... %s" % req.path)
            return

        payload = req.headers.get("AUTHORIZATION")
        if payload is None:
            raise falcon.HTTPUnauthorized(
                "Authentication required",
                description="Please provide a token")

        payload = payload.split(" ")[1]

        algo = self.cfg.get("jwt_algorithm", None)
        if algo == "RS256":
            if "jwt_pk_jwk" in self.cfg:
                secret = jwt.algorithms.RSAAlgorithm.from_jwk(
                    self.cfg["jwt_pk_jwk"])
            elif "jwt_pk_pem" in self.cfg:
                secret = self.cfg["jwt_secret_pem"]
            elif "jwt_pk_from_token" not in self.cfg:
                secret = self.resolve_token_jwks(payload)
            else:
                raise Exception("Need to specify key as jwt_secret_(jwk|pem)")
        elif algo == "HS256":
            secret = self.cfg['jwt_secret_key']
            if self.cfg['jwt_secret_encoded']:
                secret = base64.urlsafe_b64decode(secret)
            else:
                raise Exception("Need to specify secret when algo is HS256")
        else:
            raise Exception("Only HS256 and RSA256 is supported.")

        try:
            jwt_token = jwt.decode(
                payload, secret,
                algorithms=[algo],
                audience=self.cfg['jwt_audience'])
        except jwt.ExpiredSignatureError:
            raise falcon.HTTPUnauthorized(
                "JWT Error", description="Token expired")

        req.jwt = jwt_token
