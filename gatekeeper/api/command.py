# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import falcon

from gatekeeper.api import utils
from gatekeeper import access


class VerifyResource(object):
    @falcon.before(utils.deserialize)
    def on_post(self, req, resp):
        verifier = access.Validator()

        require = [
            "credential_type",
            "credential_identifier",
            "point_identifier"
        ]
        for i in require:
            if i not in req.json:
                raise falcon.HTTPBadRequest(
                    "verify_fail", "Missing %s in payload" % i)
        result = verifier.validate(
            req.json["credential_identifier"], req.json["point_identifier"])

        if not result:
            resp.status = falcon.HTTP_401
            resp.json = {"message": "Verification failed"}
        else:
            resp.status = falcon.HTTP_202
            resp.json = {"message": "Verification OK"}

        # Find the device to post the OPEN to...
