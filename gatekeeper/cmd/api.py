# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import logging
from wsgiref import simple_server

from gatekeeper.api import app
from gatekeeper.common import conf
from gatekeeper.common import service

LOG = logging.getLogger(__name__)

DEFAULTS = {
    "bind_host": "0.0.0.0",
    "bind_port": 5080,
}


def main():
    cfg = conf.load_config(prefix="GK", defaults=DEFAULTS)
    service.setup_logging(cfg)
    srv = simple_server.make_server(
        conf.CFG["bind_host"], int(conf.CFG["bind_port"]), app.get_app())
    LOG.debug("Listening on %s:%s" %
             (conf.CFG["bind_host"], conf.CFG["bind_port"]))
    srv.serve_forever()
