# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import logging
import ssl

from paho.mqtt import client

from gatekeeper import access
from gatekeeper.common import conf
from gatekeeper import controller
from gatekeeper import events

TLS_VERSIONS = {
    "1": ssl.PROTOCOL_TLSv1,
    "1.1": ssl.PROTOCOL_TLSv1_1,
    "1.2": ssl.PROTOCOL_TLSv1_2
}


LOG = logging.getLogger(__name__)


class Dispatcher(object):
    def __init__(self):
        self.validator = access.Validator()
        self.event_emitter = events.get_emitter()

    def dispatch(self, client, msg):
        command, data = msg.payload.split(";")
        args = dict([i.split("=") for i in data.split(",")])
        func = getattr(self, "on_" + command.lower())
        func(client, args)

    def on_update_status(self, client, args):
        print (args)

    def on_verify(self, client, args):
        result = self.validator.validate(args["credential"], args["point"])

        event = args.copy()
        event["name"] = "credential.verify"
        event["state"] = "denied" if not result else "granted"

        msg = "Access %(state)s to %(credential)s %(point)s@%(device)s"
        LOG.debug(msg % event)

        reply_topic = conf.CFG.mqtt_device_topic % {"device": args["device"]}
        client.publish(reply_topic, "OPEN" if result else "DENIED")

        self.event_emitter.emit(event)


class Controller(object):
    def __init__(self, *args, **kwargs):
        super(Controller, self).__init__(*args, **kwargs)
        self.client = client.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        if bool(conf.CFG["mqtt_use_tls"]):
            LOG.info("Enabling TLS for MQTT")

            self.client.tls_set(
                conf.CFG["mqtt_cafile"],
                keyfile=conf.CFG["mqtt_keyfile"],
                certfile=conf.CFG["mqtt_certfile"],
                tls_version=TLS_VERSIONS[conf.CFG["mqtt_tls_version"]])

        self.dispatcher = Dispatcher()

    def on_message(self, client, userdata, msg):
        self.dispatcher.dispatch(client, msg)

    def on_connect(self, client, userdata, flags, rc):
        LOG.debug("Connected to MQTT, subscribing...")
        client.subscribe(conf.CFG["mqtt_command_topic"])

    def run(self):
        logging.basicConfig(level="DEBUG")

        host, port = conf.CFG["mqtt_host"], int(conf.CFG["mqtt_port"])
        LOG.debug("Connecting to MQTT at %s:%d" % (host, port))
        self.client.connect(host, port)

        while True:
            self.client.loop()

    @staticmethod
    def set_defaults():
        conf.set_defaults(controller.DEFAULTS)
