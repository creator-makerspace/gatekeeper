# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import json

import requests_oauthlib
from oauthlib import oauth2


class ParticleClient(object):
    def __init__(self, client_id, client_secret,
                 api_url="https://api.particle.io"):
        self.client_id = client_id
        self.client_secret = client_secret
        self.api_url = api_url
        self.token_url = api_url + "/oauth/token"

        client = oauth2.BackendApplicationClient(self.client_id)
        self.session = requests_oauthlib.OAuth2Session(
            self.client_id, client=client)

    def request(self, url, data={}, method=None, **kwargs):
        url = self.api_url + url

        if not method:
            method = 'POST' if data else 'GET'

        if not self.session.token:
            ctype = "application/x-www-form-urlencoded;charset=UTF-8"
            headers = {
                "Content-Type": ctype
            }

            self.session.fetch_token(
                self.token_url,
                headers=headers,
                auth=(self.client_id, self.client_secret))

        headers = {}
        if data:
            data = json.dumps(data)
            headers["Content-Type"] = "application/json"
        kwargs.setdefault("headers", headers)

        response = self.session.request(method, url, data=data, **kwargs)
        return response.json()

    def get_device(self, device_id):
        return self.request("/v1/devices/%s" % device_id)

    def list_devices(self):
        return self.request("/v1/devices")

    def call_function(self, device_id, name, argument):
        url = "/v1/devices/%s/%s" % (device_id, name)
        return self.request(url, data={"args": argument}, method="POST")
