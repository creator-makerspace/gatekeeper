#    Copyright 2014 Rackspace
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
from datetime import datetime
import uuid

import six
import sqlalchemy as sa
from sqlalchemy.ext import declarative
from sqlalchemy.orm import object_mapper

from gatekeeper.db import types
from gatekeeper.db import session as db_session


class IdMixin(object):
    """Id mixin, add to subclasses that have a tenant."""
    id = sa.Column(types.UUID(), nullable=False,
                   default=lambda i: str(uuid.uuid4()),
                   primary_key=True)


class TimestampMixin(object):
    created_at = sa.Column(sa.DateTime, nullable=False,
                           default=lambda i: datetime.utcnow())
    updated_at = sa.Column(sa.DateTime, nullable=True)


class ModelIterator(six.Iterator):

    def __init__(self, model, columns):
        self.model = model
        self.i = columns

    def __iter__(self):
        return self

    # In Python 3, __next__() has replaced next().
    def __next__(self):
        n = six.advance_iterator(self.i)
        return n, getattr(self.model, n)


class Base(IdMixin):
    def __iter__(self):
        columns = list(dict(object_mapper(self).columns).keys())
        # NOTE(russellb): Allow models to specify other keys that can be looked
        # up, beyond the actual db columns.  An example would be the 'name'
        # property for an Instance.
        # columns.extend(self._extra_keys)

        return ModelIterator(self, iter(columns))

    def as_dict(self):
        d = {}
        for c in self.__table__.columns:
            d[c.name] = self[c.name]
        return d

    @classmethod
    def delete(cls, id_):
        ses = db_session.get_session()
        q = ses.query(cls)
        q = q.filter_by(id=id_)

        obj = q.one()
        if not obj:
            raise Exception
        ses.delete(obj)
        ses.flush()

    def save(self, session):
        with session.begin(subtransactions=True):
            session.add(self)
            session.flush()

BASE = declarative.declarative_base(cls=Base)
