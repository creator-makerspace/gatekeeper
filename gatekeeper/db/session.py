# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
from contextlib import contextmanager

import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker

from gatekeeper.common import conf


_engine = None
_maker = None


def get_url():
    if conf.CFG.get("db_url", None) is not None:
        return conf.CFG["db_url"]
    else:
        from sqlalchemy.engine.url import URL
        cfg = dict([(k[3:], v) for k, v in conf.CFG.items()
                   if k.startswith("db_")])
        return URL(**cfg)


def get_engine():
    global _engine

    opts = {
        "echo": True
    }

    if _engine is None:
        _engine = sa.create_engine(get_url(), **opts)
    return _engine


def get_session():
    global _maker

    if _maker is None:
        _maker = sessionmaker(
            bind=get_engine(), autocommit=True, expire_on_commit=False)

    session = _maker()
    return session


@contextmanager
def session_scope():
    session = get_session()
    try:
        yield session
    except Exception:
        raise
    finally:
        session.close()
