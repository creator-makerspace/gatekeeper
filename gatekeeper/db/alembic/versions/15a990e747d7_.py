# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
"""empty message

Revision ID: 25f54651dcd8
Revises: None
Create Date: 2015-10-31 01:31:08.145542

"""

# revision identifiers, used by Alembic.
revision = '15a990e747d7'
down_revision = None

from alembic import op
import sqlalchemy as sa

from gatekeeper.db import types


def upgrade():
    op.create_table(
        'points',
        sa.Column('created_at', sa.DateTime(), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', types.UUID(), nullable=False),
        sa.Column('name', sa.Unicode(length=100), nullable=False),
        sa.Column('identifier', sa.Unicode(length=255), nullable=False),
        sa.PrimaryKeyConstraint('id'))
    op.create_table(
        'users',
        sa.Column('created_at', sa.DateTime(), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', types.UUID(), nullable=False),
        sa.Column('name', sa.Unicode(length=100), nullable=False),
        sa.Column('enabled', sa.Boolean(), nullable=True),
        sa.PrimaryKeyConstraint('id'))
    op.create_table(
        'credentials',
        sa.Column('created_at', sa.DateTime(), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', types.UUID(), nullable=False),
        sa.Column('type', sa.Unicode(length=100), nullable=False),
        sa.Column('identifier', sa.Unicode(length=255), nullable=False),
        sa.Column('enabled', sa.Boolean(), nullable=True),
        sa.Column('user_id', types.UUID(), nullable=False),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id'))
    op.create_table(
        'grants',
        sa.Column('created_at', sa.DateTime(), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', types.UUID(), nullable=False),
        sa.Column('credential_id', types.UUID(), nullable=False),
        sa.Column('point_id', types.UUID(), nullable=False),
        sa.ForeignKeyConstraint(['credential_id'], ['credentials.id'], ),
        sa.ForeignKeyConstraint(['point_id'], ['points.id'], ),
        sa.PrimaryKeyConstraint('id', 'credential_id', 'point_id')
    )
