# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import sqlalchemy as sa
from sqlalchemy.orm import relationship

from gatekeeper.db import base
from gatekeeper.db import types


class User(base.BASE, base.TimestampMixin):
    __tablename__ = "users"
    name = sa.Column(sa.Unicode(100), nullable=False)
    enabled = sa.Column(sa.Boolean(), default=True)
    credentials = relationship("Credential", backref="user")


class Credential(base.BASE, base.TimestampMixin):
    __tablename__ = "credentials"
    type = sa.Column(sa.Unicode(100), nullable=False)
    identifier = sa.Column(sa.Unicode(255), nullable=False)
    enabled = sa.Column(sa.Boolean(), default=True)

    user_id = sa.Column(types.UUID(), sa.ForeignKey("users.id"),
                        nullable=False)
    grants = relationship("Grant", backref="credential")


class Grant(base.BASE, base.TimestampMixin):
    __tablename__ = "grants"

    credential_id = sa.Column(types.UUID(), sa.ForeignKey("credentials.id"),
                              primary_key=True, nullable=False)
    point_id = sa.Column(types.UUID(), sa.ForeignKey("points.id"),
                         primary_key=True, nullable=False)
    point = relationship("Point", backref="grants")


class Point(base.BASE, base.TimestampMixin):
    __tablename__ = "points"
    name = sa.Column(sa.Unicode(100), nullable=False)
    identifier = sa.Column(sa.Unicode(255), nullable=False)
