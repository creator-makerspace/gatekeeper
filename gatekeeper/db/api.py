# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import sqlalchemy as sa

from gatekeeper.common import exception
from gatekeeper.db import models
from gatekeeper.db import session as db_session


def _find(model, criterion=None, one=False, session=None):
    ses = session or db_session.get_session()
    q = ses.query(model)
    q = q.filter_by(**criterion)

    if one:
        try:
            return q.one()
        except sa.orm.exc.NoResultFound:
            raise exception.NotFound()
    else:
        return q.all()


def _find_one(model, criterion=None, session=None):
    return _find(model, criterion=criterion, one=True, session=session)


def _delete(model, criterion, session=None):
    ses = session or db_session.get_session()
    q = ses.query(model).filter_by(**criterion)
    q.delete()


# User
def create_user(values):
    obj = models.User(**values)

    with db_session.session_scope() as session:
        obj.save(session)
    return obj


def get_user(user_id):
    return _find_one(models.User, {"id": user_id})


def disable_user(user_id):
    with db_session.session_scope() as session:
        obj = _find_one(models.User, {"id": user_id}, session=session)
        obj.enabled = False
        obj.save(session)


# Point
def create_point(values):
    obj = models.Point(**values)

    with db_session.session_scope() as session:
        obj.save(session)
    return obj


def get_point(point_id):
    return _find_one(models.Point, {"id": point_id})


def find_point(criterion):
    return _find_one(models.Point, criterion)


def delete_point(point_id):
    criterion = {
        "id": point_id
    }
    _delete(models.Point, criterion)


# Credential
def create_credential(values):
    obj = models.Credential(**values)

    with db_session.session_scope() as session:
        obj.save(session)
    return obj


def get_credential(credential_id):
    criterion = {
        "id": credential_id
    }
    return _find_one(models.Credential, criterion)


def find_credential(criterion):
    return _find_one(models.Credential, criterion)


def delete_credential(credential_id):
    criterion = {
        "id": credential_id
    }
    _delete(models.Credential, criterion)


def disable_credential(credential_id):
    with db_session.session_scope() as session:
        obj = _find_one(models.Credential,
                        {"id": credential_id}, session=session)
        obj.enabled = False
        obj.save(session)


# Grant
def create_grant(values):
    criterion = {
        "id": values["credential_id"]
    }
    credential = _find_one(models.Credential, criterion)

    with db_session.session_scope() as session:
        grant = models.Grant(
            credential_id=credential.id,
            point_id=values["point_id"])
        session.add(grant)
        session.flush()

    return grant


def get_grant(grant_id):
    criterion = {
        "id": grant_id
    }
    return _find_one(models.Grant, criterion)


def find_grant(criterion):
    return _find_one(models.Grant, criterion)


def delete_grant(grant_id):
    criterion = {
        "id": grant_id
    }
    _delete(models.Grant, criterion)
