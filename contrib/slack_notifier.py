# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import logging
import time

from oslo_config import cfg
from oslo_log import log
import rethinkdb as rdb
import requests

from gatekeeper.common import service


opts = [
    cfg.StrOpt("gatekeeper_url", default="http://localhost:5080"),
    cfg.StrOpt("channel", default="#whos-home"),
    cfg.StrOpt("username", default="gatekeeper"),
    cfg.StrOpt("message", default="PSSST! %(name)s has arrived at Creator!"),
    cfg.StrOpt("webhook_url")
]

cfg.CONF.register_opts(opts, group="slack_notifier")

LOG = log.getLogger(__name__)


conn = None


def get_rdb():
    global conn
    if conn is None:
        conn = rdb.connect(
            cfg.CONF.rethinkdb.host,
            cfg.CONF.rethinkdb.port,
            db=cfg.CONF.rethinkdb.database)
    return conn


def get_user_from_credential(cred):
    # Currently the notifcation comes with the identifier of the cred
    # and not the id * NEED TO FIX *
    url = cfg.CONF.slack_notifier.gatekeeper_url + '/credentials'
    credentials = requests.get(url).json()
    user_cred = None
    for c in credentials["credentials"]:
        if c["identifier"] == cred:
            user_cred = c
            break
    if user_cred is None:
        return

    url = cfg.CONF.slack_notifier.gatekeeper_url + '/users'
    users = requests.get(url).json()
    for u in users["users"]:
        if u["id"] == user_cred["user_id"]:
            return u


def notify_slack(user):
    msg = cfg.CONF.slack_notifier.message % {"name": user["name"]}
    data = {
        "channel": cfg.CONF.slack_notifier.channel,
        "username": cfg.CONF.slack_notifier.username,
        "text": msg
    }
    requests.post(cfg.CONF.slack_notifier.webhook_url, json=data)


def main():
    service.prepare()
    cfg.CONF.log_opt_values(LOG, logging.DEBUG)
    LOG.debug("Starting slack notifier...")
    while True:
        try:
            c = get_rdb()
        except rdb.ReqlError as e:
            time.sleep(4)
            LOG.exception(e)
            continue

        feed = rdb.table("events").changes().run(c)
        for c in feed:
            if c["state"] == "granted":
                user = get_user_from_credential(c["new_val"]["credential"])
                if user is None:
                    continue
                notify_slack(user)


if __name__ == "__main__":
    main()
