# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import argparse
import logging

import falcon
import requests

from gatekeeper.api import utils
from gatekeeper.common import conf
from gatekeeper.common import service

from wsgiref import simple_server

DEFAULTS = {
    "host": "0.0.0.0",
    "port": 5081,
    "api_url": "http://localhost:5080",
}

conf.set_defaults(DEFAULTS)

LOG = logging.getLogger(__name__)

api = falcon.API(
    request_type=utils.Request,
    response_type=utils.Response)


class CommandError(Exception):
    pass


def get_points():
    response = requests.get(
        conf.CFG["gatekeeper_url"] + "/points")
    return response.json()["points"]


class SlackParser(argparse.ArgumentParser):
    def error(self, error):
        raise CommandError(error)


class SlackResource(object):
    def on_post(self, req, resp):
        LOG.debug("Received slack req %s" % req.params)
        try:
            if req.params["token"] != conf.CFG["slack_token"]:
                raise falcon.HTTPForbidden(
                    "not_authed", "Token was not present in request")
        except KeyError:
            raise falcon.HTTPBadRequest(
                "bad_request", "Invalid request")

        if "text" in req.params:
            arg_data = req.params["text"].split(" ")
        else:
            arg_data = []

        parser = SlackParser(add_help=False, prog="/doors")
        cmd_parser = parser.add_subparsers(dest="cmd")

        cmd_parser.add_parser("help", help="Show this message")
        cmd_parser.add_parser("list", help="List all secret passages")
        open_parser = cmd_parser.add_parser("open", help="Open sesame?")
        open_parser.add_argument("door_name", help="Door to open")

        try:
            args = parser.parse_args(arg_data)
        except CommandError as e:
            resp.body = "Error: %s" % e.message
            return

        LOG.debug("Command is %s", args.cmd)

        if args.cmd == "list":
            resp.body = "Doors present: \n%s" % ", ".join(
                [i["name"] for i in get_points()])
        elif args.cmd == "open":
            point = None
            points = get_points()
            for p in points:
                if p["name"] == args.door_name:
                    point = p
                    break
            else:
                resp.body = "Door not found..."
                return

            payload = {
                "credential_identifier": req.params["user_id"],
                "credential_type": "slack",
                "point_identifier": point["identifier"]
            }

            response = requests.post(
                conf.CFG["gatekeeper_url"] + "/verify",
                json=payload)

            if response.json():
                resp.body = response.json()["message"]
        elif args.cmd == "help":
            resp.body = parser.format_usage()


api.add_route("/", SlackResource())


if __name__ == '__main__':
    service.prepare()
    srv = simple_server.make_server(
        conf.CFG["host"], conf.CFG["port"], api)
    srv.serve_forever()
