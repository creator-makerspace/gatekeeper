# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import falcon
from oslo_serialization import jsonutils

from gatekeeper.db import api
from tests.functional import v1


class GrantsTest(v1.V1Test):
    def setUp(self):
        super(GrantsTest, self).setUp()
        data = {
            "name": "jdue"
        }

        # setup a user
        self.user = api.create_user(data)

        credential = {
            "identifier": "random",
            "type": "mycard",
            "user_id": self.user.id
        }

        # setup credential
        self.credential = api.create_credential(credential)

        self.points = [
            api.create_point(
                {"name": "door%s" % i, "identifier": i}) for i in xrange(0, 2)
        ]

    def test_create(self):
        data = {
            "credential_id": self.credential.id,
            "point_id": self.points[0].id
        }

        url = "/grants"
        response = self.simulate_post(url, body=data)
        body = jsonutils.loads(response[0])

        self.assertEqual(self.srmock.status, falcon.HTTP_201)
        self.assertEqual(data["credential_id"], body["credential_id"])
        self.assertEqual(data["point_id"], body["point_id"])
        self.assertIn("id", body)
        self.assertIn("updated_at", body)
        self.assertIn("created_at", body)

    def test_get(self):
        data = {
            "credential_id": self.credential.id,
            "point_id": self.points[0].id
        }
        obj = api.create_grant(data)

        url = "/grants/%s" % obj.id
        response = self.simulate_get(url)
        body = jsonutils.loads(response[0])

        self.assertEqual(self.srmock.status, falcon.HTTP_200)
        self.assertEqual(data["credential_id"], body["credential_id"])
        self.assertEqual(data["point_id"], body["point_id"])
        self.assertIn("id", body)
        self.assertIn("updated_at", body)
        self.assertIn("created_at", body)

    def test_list(self):
        items = [
            {
                "point_id": self.points[0].id,
                "credential_id": self.credential.id
            },
            {
                "point_id": self.points[1].id,
                "credential_id": self.credential.id
            }
        ]

        for i in xrange(0, len(items)):
            api.create_grant(items[i])

        url = "/grants"
        response = self.simulate_get(url)
        body = jsonutils.loads(response[0])

        self.assertEqual(self.srmock.status, falcon.HTTP_200)
        self.assertEqual(len(body["grants"]), len(items))

        for i in xrange(0, len(items)):
            item = body["grants"][i]
            self.assertEqual(items[i]["credential_id"], item["credential_id"])
            self.assertEqual(items[i]["point_id"], item["point_id"])
            self.assertIn("id", item)
            self.assertIn("updated_at", item)
            self.assertIn("created_at", item)

    def test_delete(self):
        data = {
            "credential_id": self.credential.id,
            "point_id": self.points[0].id
        }
        obj = api.create_grant(data)

        url = "/grants/%s" % obj.id
        self.simulate_delete(url)

        self.assertEqual(self.srmock.status, falcon.HTTP_204)
