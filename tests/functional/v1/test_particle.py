# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import ddt
import falcon
from oslo_serialization import jsonutils as json
import mock

from gatekeeper.common import conf
from tests.functional import v1


@ddt.ddt
class ParticleTest(v1.V1Test):
    def setUp(self):
        super(ParticleTest, self).setUp()
        conf.CFG["client_id"] = "id"
        conf.CFG["client_secret"] = "secret"

    @ddt.data(
        ({
            "coreid": "1234",
            "data": {"event": "verify", "credential_id": "4321"},
        }, True, "OPEN"),
        ({
            "coreid": "1234",
            "data": {"event": "verify", "credential_id": "4321"},
        }, False, "DENIED")
    )
    @mock.patch("gatekeeper.access.Validator.validate")
    @mock.patch("gatekeeper.events.get_emitter")
    @mock.patch("gatekeeper.particle_client.ParticleClient.call_function")
    def test_verify(self, args, mock_particle_func, mock_get_emitter,
                    mock_validate):
        mock_emitter = mock.Mock()
        mock_get_emitter.return_value = mock_emitter

        conf.CFG["particle_webhook_token"] = "1234"
        self.headers["X-Auth-Token"] = "1234"

        hook_event, result, cmd = args

        mock_validate.return_value = result

        payload = hook_event.copy()
        payload["data"] = json.dumps(payload.pop("data"))
        self.simulate_post("/particle", body=payload)

        self.assertEqual(self.srmock.status, falcon.HTTP_200)

        mock_validate.assert_called_with(
            hook_event["data"]["credential_id"], hook_event["coreid"])

        event = {
            "device_id": payload["coreid"],
            "point_id": payload["coreid"],
            "credential_id": hook_event["data"]["credential_id"],
            "event": "credential.verify",
            "state": "granted" if result else "denied"
        }

        mock_emitter.emit.assert_called_with(event)

        mock_particle_func.assert_called_with(
            hook_event["coreid"], "command", cmd)

    @ddt.data(None, "foo")
    @mock.patch("gatekeeper.events.get_emitter")
    def test_verify_401(self, token, _):
        if token is not None:
            self.headers["X-Auth-Token"] = token
        response = self.simulate_post("/particle", body={})

        data = json.loads(response[0])
        self.assertTrue("error" in data)
