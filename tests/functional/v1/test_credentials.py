# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import falcon
from oslo_serialization import jsonutils

from gatekeeper.db import api
from tests.functional import v1


class CredentialsTest(v1.V1Test):
    def setUp(self):
        super(CredentialsTest, self).setUp()
        data = {
            "name": "jdue"
        }

        self.user = api.create_user(data)

    def test_create(self):
        data = {
            "identifier": "random",
            "type": "card",
            "user_id": self.user.id
        }

        url = "/credentials"
        response = self.simulate_post(url, body=data)
        body = jsonutils.loads(response[0])

        self.assertEqual(self.srmock.status, falcon.HTTP_201)
        self.assertEqual(data["identifier"], body["identifier"])
        self.assertEqual(data["type"], body["type"])
        self.assertIn("id", body)
        self.assertEqual(data["user_id"], body["user_id"])
        self.assertIn("updated_at", body)
        self.assertIn("created_at", body)
        self.assertIn("created_at", body)

    def test_get(self):
        data = {
            "identifier": "random",
            "type": "card",
            "user_id": self.user.id
        }

        obj = api.create_credential(data)

        url = "/credentials/%s" % (obj.id)
        response = self.simulate_get(url)
        body = jsonutils.loads(response[0])

        self.assertEqual(self.srmock.status, falcon.HTTP_200)
        self.assertEqual(data["identifier"], body["identifier"])
        self.assertEqual(data["type"], body["type"])
        self.assertIn("id", body)
        self.assertIn("updated_at", body)
        self.assertIn("created_at", body)
        self.assertIn("created_at", body)

    def test_list(self):
        items = [
            {
                "identifier": "random",
                "type": "card",
                "user_id": self.user.id
            },
            {
                "identifier": "random2",
                "type": "card",
                "user_id": self.user.id
            }
        ]

        for i in items:
            api.create_credential(i)

        response = self.simulate_get("/credentials")
        body = jsonutils.loads(response[0])

        self.assertEqual(self.srmock.status, falcon.HTTP_200)

        for i in xrange(0, len(items)):
            item = body["credentials"][i]
            self.assertEqual(items[i]["identifier"], item["identifier"])
            self.assertEqual(items[i]["type"], item["type"])
            self.assertIn("id", item)
            self.assertIn("updated_at", item)
            self.assertIn("created_at", item)
            self.assertIn("created_at", item)

    def test_delete(self):
        data = {
            "identifier": "random",
            "type": "card",
            "user_id": self.user.id
        }

        obj = api.create_credential(data)

        url = "/credentials/%s" % obj.id
        self.simulate_delete(url)

        self.assertEqual(self.srmock.status, falcon.HTTP_204)
