# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import falcon
from oslo_serialization import jsonutils

from gatekeeper.db import api
from tests.functional import v1


class PointsTest(v1.V1Test):
    def setUp(self):
        super(PointsTest, self).setUp()

    def test_create(self):
        data = {
            "name": "door1",
            "identifier": "1"
        }

        url = "/points"
        response = self.simulate_post(url, body=data)
        body = jsonutils.loads(response[0])

        self.assertEqual(self.srmock.status, falcon.HTTP_201)
        self.assertEqual(data["name"], body["name"])
        self.assertIn("id", body)
        self.assertIn("updated_at", body)
        self.assertIn("created_at", body)

    def test_get(self):
        data = {
            "name": "door1",
            "identifier": "1"
        }
        obj = api.create_point(data)

        url = "/points/%s" % obj.id
        response = self.simulate_get(url)
        body = jsonutils.loads(response[0])

        self.assertEqual(self.srmock.status, falcon.HTTP_200)
        self.assertEqual(obj.id, body["id"])
        self.assertEqual(data["name"], body["name"])
        self.assertIn("updated_at", body)
        self.assertIn("created_at", body)

    def test_list(self):
        items = [
            {
                "name": "door1",
                "identifier": "1"
            },
            {
                "name": "door2",
                "identifier": "2"
            }
        ]

        for i in xrange(0, len(items)):
            api.create_point(items[i])

        url = "/points"
        response = self.simulate_get(url)
        body = jsonutils.loads(response[0])

        self.assertEqual(self.srmock.status, falcon.HTTP_200)
        self.assertEqual(len(body["points"]), len(items))

        for i in xrange(0, len(items)):
            item = body["points"][i]
            self.assertIn("id", item)
            self.assertIn("updated_at", item)
            self.assertIn("created_at", item)

    def test_delete(self):
        data = {
            "name": "door1",
            "identifier": "1"
        }
        obj = api.create_point(data)

        url = "/points/%s" % obj.id
        self.simulate_delete(url)

        self.assertEqual(self.srmock.status, falcon.HTTP_204)
