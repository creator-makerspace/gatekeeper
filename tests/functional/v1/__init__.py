from gatekeeper.common import conf
from tests.functional import base


class V1Test(base.BaseTest):
    def setUp(self):
        conf.CFG["auth_enable"] = False
        super(V1Test, self).setUp()
