# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import logging

from falcon import testing
from oslo_serialization import jsonutils
import testtools

from gatekeeper.api import app
from gatekeeper.common import conf
from tests.functional.fixtures import database as database_fixture


_DB_CACHE = None

logging.basicConfig(level="DEBUG")


class BaseTest(testtools.TestCase):
    def setUp(self):
        super(BaseTest, self).setUp()

        conf.CFG["db_url"] = "sqlite://"

        self.headers = {
            "content-type": "application/json"
        }

        # Simulate WSGI
        self.app = app.get_app()
        self.srmock = testing.StartResponseMock()

        self._include_default_fixtures()

    def _include_default_fixtures(self):
        global _DB_CACHE
        if not _DB_CACHE:
            _DB_CACHE = database_fixture.Database(
                conf.CFG,
                sqlite_db="/tmp/gatekeeper-tests.sqlite",
                sqlite_clean_db="/tmp/gatekeeper-clean.sqlite",
            )
        self.useFixture(_DB_CACHE)

    def simulate_request(self, path, **kwargs):
        headers = kwargs.setdefault("headers", self.headers).copy()
        kwargs["headers"] = headers

        if "body" in kwargs and isinstance(kwargs["body"], dict):
            kwargs["body"] = jsonutils.dumps(kwargs["body"])

        env = testing.create_environ(path=path, **kwargs)
        return self.app(env, self.srmock)

    def simulate_get(self, *args, **kwargs):
        kwargs['method'] = 'GET'
        return self.simulate_request(*args, **kwargs)

    def simulate_post(self, *args, **kwargs):
        kwargs['method'] = 'POST'
        return self.simulate_request(*args, **kwargs)

    def simulate_put(self, *args, **kwargs):
        kwargs['method'] = 'PUT'
        return self.simulate_request(*args, **kwargs)

    def simulate_patch(self, *args, **kwargs):
        kwargs['method'] = 'PATCH'
        return self.simulate_request(*args, **kwargs)

    def simulate_delete(self, *args, **kwargs):
        kwargs['method'] = 'DELETE'
        return self.simulate_request(*args, **kwargs)

    def simulate_options(self, *args, **kwargs):
        kwargs['method'] = 'OPTIONS'
        return self.simulate_request(*args, **kwargs)
