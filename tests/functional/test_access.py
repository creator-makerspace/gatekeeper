# -*- encoding: utf-8 -*-
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
from gatekeeper import access
from gatekeeper.db import api
from tests.functional import base


class AccessTest(base.BaseTest):
    def setUp(self):
        super(AccessTest, self).setUp()
        self.user = api.create_user({"name": "jdoe"})

        credential = {
            "user_id": self.user.id,
            "type": "card",
            "identifier": "123"
        }

        self.credential = api.create_credential(credential)

        self.point = api.create_point(
            {
                "name": "door",
                "identifier": "1"
            }
        )

        self.validator = access.Validator()

    def test_user_disabled(self):
        api.disable_user(self.user.id)

        result = self.validator.validate(self.credential.id, self.point.id)
        self.assertFalse(result)

    def test_credential_disabled(self):
        api.disable_credential(self.credential.id)
        result = self.validator.validate(self.credential.id, self.point.id)
        self.assertFalse(result)

    def test_granted(self):
        grant_data = {
            "credential_id": self.credential.id,
            "point_id": self.point.id
        }

        api.create_grant(grant_data)

        self.validator.validate(self.credential.id, self.point.id)

    def test_invalid_point(self):
        grant_data = {
            "credential_id": self.credential.id,
            "point_id": self.point.id
        }

        api.create_grant(grant_data)

        self.validator.validate(
            self.credential.identifier, "032dd7ee-0796-4b5c-b1ac-383bea74d75b")
