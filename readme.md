# Create db <br>
mysql -uroot -prootp4ss -e 'CREATE DATABASE gatekeeper' <br>

# Sync <br>
gatekeeper-manage --config-file etc/gatekeeper.ini database upgrade <br>

# Building the Docker image <br>
sudo docker build -t ekarlso/gatekeper .<br>

# Running database migrations <br>
sudo docker run -v $PWD/etc:/config gatekeeper gatekeeper-manage --config-file /config/gatekeeper.ini database upgrade <br>

# Run the controller (Receives events via MQTT) <br>
sudo docker run -v $PWD/etc:/config gatekeeper gatekeeper-controller --config-file /config/gatekeeper.ini <br>

# Run the API (To configure users etc)<br>
sudo docker run -v $PWD/etc:/config gatekeeper gatekeeper-api --config-file /config/gatekeeper.ini <br>

# If you want to push the to dockerhub the current repo is ekarlso/gatekeeper
sudo docker push ekarlso/gatekeeper
